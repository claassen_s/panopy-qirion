# !/usr/bin/env python
#
# ML 2021-02-16 Reworked code
#

import signal
import sys
import time
from Packages.DataWattch import domoticz, Devices as Dev
from Packages.RPiGPIO.RPi import GPIO as GPIOi

idx0 = 0
idx1 = 0


def linux_signal_handler(sig, frame):
    GPIOi.cleanup()
    sys.exit(0)


def physical_shutdown_button_handler(channel):
    print("Shutdown Button: ", GPIOi.input(22))
    print(channel)


def dz_update_counter(channel):
    print(channel)
    if channel == 17 and int(idx0) > 0:
        dom_comm.set_counter(idx0, 1)

    if channel == 27 and int(idx1) > 0:
        dom_comm.set_counter(idx1, 1)


if __name__ == "__main__":
    dom_comm = domoticz()
    error = ''
    GPIOi.setmode(GPIOi.BCM)

    # Set Pins As Input
    GPIOi.setup(17, GPIOi.IN, pull_up_down=GPIOi.PUD_DOWN)
    GPIOi.setup(27, GPIOi.IN, pull_up_down=GPIOi.PUD_DOWN)
    GPIOi.setup(22, GPIOi.IN, pull_up_down=GPIOi.PUD_DOWN)

    # Enable Interrupt Handler
    GPIOi.add_event_detect(17, GPIOi.FALLING, callback=dz_update_counter, bouncetime=100)
    GPIOi.add_event_detect(27, GPIOi.FALLING, callback=dz_update_counter, bouncetime=100)

    """
    Shutdown Button Functionality isn't operational yet 
    GPIOi.add_event_detect(22, GPIOi.FALLING, callback=physical_shutdown_button_handler, bouncetime=100)
    """

    # Get Domoticz Devices
    try:
        (counters, devices) = dom_comm.get_settings()
        idx0 = counters[0].idx
        idx1 = counters[1].idx

        print("idx0=", idx0)
        print("idx1=", idx1)

    except KeyError:
        print("DZ Connection failed: could not get Devices (Are they defined?)")

    signal.signal(signal.SIGINT, linux_signal_handler)
    signal.pause()
