#!/usr/bin/env python

# Release 0.2/
# Author D. Driessen
# Copyright (c) 2020  Ascos
# Python 3

###########################################################################
#
#  Opmerking Martin 2021-04-01
#	- Dit is een speciale versie van scheduler voor Talis
#	- HARDWARE SPECIFIEK: PullDown init ipv PullUp
#	- Functies:
#		- Schakelen op Temp
#		- Schakelen op Amps
#		- Push melding bij schakeling
#
###########################################################################

import subprocess
import time
import requests
import os
from typing import List

from smbus2 import SMBus, i2c_msg
from Packages.DataWattch import domoticz, Devices as Dev
from Packages.RPiGPIO.RPi import GPIO as GPIOi
from Packages.DataWattch import stm_communication as stm

# Setup
bus = SMBus(1)
address = 0x48
last_push_sent_at = 0
limit_push_notifications = 10

# Get our serial number
f = open("/home/ubuntu/.panoid")
serial = f.read(14)
f.close()

# IDX Setup (will be overwritten by User Vars)
idx_temp1 = 42
idx_temp2 = 43
idx_relay = 44


def init_values(dom_class: domoticz, cnt: List[Dev.DeviceCounter]):
    dom_class.init_counter_values(cnt)
    pass


if __name__ == "__main__":
    ##############################################################################################################
    # Initialization
    ##############################################################################################################
    dom_comm = domoticz()
    error = ''

    # Set board to run in Execution mode, rather than Programming/Debug Mode
    GPIOi.cleanup()
    GPIOi.setmode(GPIOi.BCM)
    GPIOi.setup(8, GPIOi.OUT)  # output
    GPIOi.setup(7, GPIOi.OUT)  # output
    time.sleep(0.2)
    GPIOi.output(8, 0)  # output 1
    GPIOi.output(7, 1)  # output 1
    time.sleep(0.2)
    GPIOi.output(7, 0)  # output 0
    time.sleep(2)

    # Get Domoticz User Variables
    relay = dom_comm.get_relay("relay")
    temp1_sensorpin = int(dom_comm.get_uservarbyname("temp1_pin"))
    temp2_sensorpin = int(dom_comm.get_uservarbyname("temp2_pin"))
    temp_min = float(dom_comm.get_uservarbyname("temp_min"))
    temp_max = float(dom_comm.get_uservarbyname("temp_max"))
    temp_time = int(dom_comm.get_uservarbyname("temp_time"))
    amp_max = float(dom_comm.get_uservarbyname("amp_max"))
    amp_min = float(dom_comm.get_uservarbyname("amp_min"))
    amp_time = int(dom_comm.get_uservarbyname("amp_time"))

    # Initialization for Temperature Sensors
    stm.interrupt_enable(temp1_sensorpin, 'falling', 0)
    stm.interrupt_enable(temp2_sensorpin, 'falling', 1)
    stm.dcycleinEnable(temp1_sensorpin)
    stm.dcycleinEnable(temp2_sensorpin)
    stm.set_interrupts()
    stm.setdcycle()

    (counters, devices) = dom_comm.get_settings()
    for i in counters:
        print(i.__dict__)

    (volt, power, gas) = dom_comm.get_P1_init()
    init_values(dom_comm, counters)
    outphase = [Dev.DeviceOutputSimple(0), Dev.DeviceOutputSimple(1)]
    Dev.send_init(["output"])
    level = 0
    t_start = 0
    t_startLED = 0
    rly_state = 0
    amp = [0, 0, 0]

    sentPushEarlier = False

    # Set IDX's based on User Variables
    try:
        idx_temp1 = int(dom_comm.get_uservarbyname("temp1_idx"))
        idx_temp2 = int(dom_comm.get_uservarbyname("temp2_idx"))
        idx_relay = int(dom_comm.get_uservarbyname("relay_idx"))
    except TypeError as err:
        raise SystemExit("DW Fatal: Temperature and/or Relay IDX not defined in Domoticz. Temp2 MUST be defined.")

    while True:

        # Sleep for 2 seconds at the start of each iteration
        print()
        print("#######################################################")
        print("Tick", time.time())
        print("#######################################################")
        print()

        t_end = time.time() + 2
        while time.time() < t_end:
            pass

#       pairbutton.get_button() moet nog worden gemaakt
#       print("Pair button status : ", pairbutton)
        for P1_def in range(len(volt)):
            volt[P1_def].value = dom_comm.get_data(volt[P1_def].idx)

        for P1_def in range(len(power)):
            power[P1_def].value = dom_comm.get_data(power[P1_def].idx)

        for P1_def in range(len(gas)):
            gas[P1_def].value = dom_comm.get_data(gas[P1_def].idx)

        for i in range(len(power)):
            for j in range(len(volt)):
                if volt[j].fase == power[i].fase:
                    if volt[j].value > 1000:
                        amp[i] = power[i].value / (volt[j].value / 10.0)
                    else:
                        amp[i] = power[i].value / volt[j].value

                    print('Power:', power[i].value, 'Voltage:', volt[j].value, 'Amps:', amp[i])

        for i in range(0, len(amp)):
            amp[i] = amp[i]

        print()

        # Update temperature values in DZ
        dom_comm.set_temp(idx_temp1, stm.get_temp(temp1_sensorpin))
        print("Temp", temp1_sensorpin, ":", stm.get_temp(temp1_sensorpin))
        dom_comm.set_temp(idx_temp2, stm.get_temp(temp2_sensorpin))
        print("Temp", temp2_sensorpin, ":", stm.get_temp(temp2_sensorpin))

        ##############################################################################################################
        # Events: Turn ON Relay (= Turn OFF water heater)
        ##############################################################################################################
        # Above temperature limit (but below 150 centigrade; likely faulty thermometer readout)
        switchedByAmps = False

       # if 150 > stm.get_temp(temp1_sensorpin) > temp_max and dom_comm.get_relay("relay") == False:
            # Confirm temperature stays above [TEMP_MAX] degrees for [TEMP_TIME] seconds
       #     for counter in range(0, temp_time):
       #         if stm.get_temp(temp1_sensorpin) >= temp_max:
       #             time.sleep(1)
       #             print("[TEMP TOO HIGH]:", stm.get_temp(temp1_sensorpin))

            # If it went under again, cancel
       #     if stm.get_temp(temp1_sensorpin) < temp_max:
       #         continue

            # Switch on the relay (meaning switch off the water heater)
       #     else:
                # Set expected Relay state to 1
       #         level = 1

                # Switch on the Light
       #         t_startLED = time.time()
       #         error = outphase[1].set_level(1)

       #         print("TEMP-RELAY: AAN")
       #         dom_comm.set_switch(idx_relay, "On")

        # Above Amps limit
        if max(amp) > amp_max and dom_comm.get_relay("relay") == False:

            # Confirm Amps stays above [AMP_MAX] degrees for [AMP_TIME] seconds
            for counter in range(0, amp_time):
                if max(amp) >= amp_max:
                    time.sleep(1)
                    print("[AMP TOO HIGH]:", max(amp))
                    switchedByAmps = True

            # If it went under again, cancel
            if max(amp) < amp_max:
                continue

            # Switch on the relay (meaning switch off the water heater)
            else:
                # Set expected Relay state to 1
                level = 1

                # Switch on the Light
                t_startLED = time.time()
                error = outphase[1].set_level(1)

                print("AMP-RELAY: AAN")
                dom_comm.set_switch(idx_relay, "On")

                # Send a Push Notification via PANINE
                if switchedByAmps:
                    if last_push_sent_at < time.time() - limit_push_notifications:
                        print("Send push that relay is ON")
                        os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=ON")
                        last_push_sent_at = time.time()
                        sentPushEarlier = True

        ##############################################################################################################
        # Events: Turn OFF Relay (= Turn ON water heater)
        ##############################################################################################################
        # Below Amps Limit AND Below Temperature Limit
        #if stm.get_temp(temp1_sensorpin) < temp_min and max(amp) < amp_min and dom_comm.get_relay("relay") == True: Changed by Maickel to not switch on temp
        if max (amp) < amp_min and dom_comm.get_relay("relay") == True:
            switchedByAmps = False

            # Confirm Temperature stays under [TEMP_MIN] for [TEMP_TIME] seconds Changed by Maickel
            #for counter in range(0, temp_time):
            #    if stm.get_temp(temp1_sensorpin) <= temp_min:
            #        time.sleep(1)
            #        print("[TEMP low enough again]:", stm.get_temp(temp1_sensorpin))

            # Confirm Amps stays under [AMP_MIN] for [AMP_TIME] seconds
            for counter in range(0, temp_time):
                if max(amp) <= amp_min:
                    time.sleep(1)
                    print("[AMP low enough again]:", max(amp))
                    switchedByAmps = True

            # Cancel if the conditions are still the same
            if max(amp) > amp_min:
                continue
            #elif stm.get_temp(temp1_sensorpin) > temp_min: changed by Maickel
            #    continue

            # Code to actually switch the relay OFF
            else:
                # Set expected Relay state to 0
                level = 0

                print("switchedByAmps=", switchedByAmps, " sentPushEarlier=",  sentPushEarlier)

                # Send a Push Notification via PANINE
                if switchedByAmps and sentPushEarlier:
                    if last_push_sent_at < time.time() - limit_push_notifications:
                        print("Send push that relay is OFF")
                        os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=OFF")
                        last_push_sent_at = time.time()
                        sentPushEarlier = False

                print("RELAY: UIT")
                dom_comm.set_switch(idx_relay, "Off")

        ##############################################################################################################
        # Code to run every tick (every 2 seconds)
        ##############################################################################################################
        # Set the relay to the state we expect it to be
        outphase[0].set_level(level)

        print()
        print("Relay (according to var):", level)

        # A physical LED light will glow for 15 minutes
        # This code switches the light OFF again after those 15 minutes
        if t_startLED < (time.time() - (15 * 60)) and not dom_comm.get_relay("relay"):
            outphase[1].set_level(0)

        print("Relay (according to DZ): ", dom_comm.get_relay("relay"))

        # Update kWh values in Domoticz
        for Dev in range(len(counters)):
            error = counters[Dev].get_counter_value()
            if counters[Dev].update > 0:
                dom_comm.set_counter(counters[Dev].idx, counters[Dev].update)
                counters[Dev].update = 0
            if error:
                dom_comm.add_log(error)
            time.sleep(0.1)
