# !/usr/bin/env python

# Manually install the missing dependencies:
# pip3 install requests
# pip3 install smbus2


import subprocess
import time
import requests
from typing import List

from smbus2 import SMBus, i2c_msg
from Packages.DataWattch import domoticz, Devices as Dev
from Packages.DataWattch import stm_communication as stm
from Packages.RPiGPIO.RPi import GPIO as GPIOi

bus = SMBus(1)
address = 0x48
temp_sensorpin = 3

# Get our serial number
f = open("/home/ubuntu/.panoid")
serial = f.read(14)
f.close()


# wait till domoticz server has started
#function doesn't work
def wait_domoticz():
    ovf = time.time()
    service = 'domoticz'
    p = subprocess.Popen(["systemctl", "is-active", service], stdout=subprocess.PIPE)
    (output, err) = p.communicate()
    stat = output.decode("UTF-8")
    # wait service status, 2 minutes timeout counter
    while stat != 'active' and ovf > (time.time() - 120):
        time.sleep(100)
        (output, err) = p.communicate()
        stat = output.decode("UTF-8")

    # service still not started - restart service
    if stat != 'active':
        p = subprocess.Popen(["systemctl", "restart", service], stdout=subprocess.PIPE)
        # wait till service is started (~2 min)
        time.sleep(120)
    pass


def init_values(dom_class: domoticz, cnt: List[Dev.DeviceCounter]):
    dom_class.init_counter_values(cnt)
    pass


if __name__ == "__main__":
    #GPIOi.setup(7, GPIOi.OUT, pull_up_down=GPIOi.PUD_DOWN)
    amp_limit = 25  # change here to change amperage
    delay = 5  # change this value for delay time (in seconds)

    dom_comm = domoticz()
    error = ''
    # retrieve domoticz settings
    (counters, devices) = dom_comm.get_settings()

    (volt, power, gas) = dom_comm.get_P1_init()
    init_values(dom_comm, counters)
    pairbutton = Dev.SystemDevice()
    print("Pair button status : ", pairbutton)
    # create output device
    outphase = [Dev.DeviceOutputSimple(0)]

    Dev.send_init(["output"])
    level = 0
    t_start = 0
    rly_state = 0
    amp = [0, 0, 0]
    while True:

        # Do this for 2 sec
        t_end = time.time() + 2
        while time.time() < t_end:
            pass
        #           if level == 1:
        #               error = test_output.set_level(0)
        #               level = 0
        #           else:
        #               error = test_output.set_level(1)
        #               level = 1
        # wait atleast 100 millis
        #           time.sleep(0.1)
        pairbutton.get_button()
        for P1_def in range(len(volt)):
            volt[P1_def].value = dom_comm.get_data(volt[P1_def].idx)

        for P1_def in range(len(power)):
            power[P1_def].value = dom_comm.get_data(power[P1_def].idx)

        for P1_def in range(len(gas)):
            gas[P1_def].value = dom_comm.get_data(gas[P1_def].idx)

        for i in range(len(power)):
            for j in range(len(volt)):
                if volt[j].fase == power[i].fase:
                    if volt[j].value > 1000:
                        amp[i] = power[i].value / (volt[j].value / 10.0)
                    else:
                        amp[i] = power[i].value / volt[j].value

                    print('power:', power[i].value, 'voltage:', volt[j].value, 'amp:', amp[i])

        if max(amp) > amp_limit:
            if t_start == 0 and rly_state == 0:
                t_start = time.time()
                rly_state = 2
            elif 0 < t_start < (time.time() - delay):
                level = 1
                #error = outphase[0].set_level(1)
                rly_state = 1
                t_start = 0
                requests.get('https://app.datawattch.com/prototalis?d=' + serial + '&state=off')
            elif rly_state == 3:
                t_start = 0
                rly_state = 1
        else:
            if t_start == 0 and rly_state == 1:
                t_start = time.time()
                rly_state = 3
            elif 0 < t_start < (time.time() - delay):
                #error = outphase[0].set_level(0)
                level = 0
                rly_state = 0
                t_start = 0
                requests.get('https://app.datawattch.com/prototalis?d=' + serial + '&state=on')
            elif rly_state == 2:
                t_start = 0
                rly_state = 0
        error = outphase[0].setlevel(level)
                # set pulse counter

        for Dev in range(len(counters)):
            error = counters[Dev].get_counter_value()
            if counters[Dev].update > 0:
                print('Dev:', Dev, 'val:', counters[Dev].update)
                dom_comm.set_counter(counters[Dev].idx, counters[Dev].update)
                counters[Dev].update = 0
            if error:
                dom_comm.add_log(error)
            time.sleep(0.1)
