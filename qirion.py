# Version RC 1.0
# First release candidate version
# By Stijn Claassen
# Qirion Energy Consulting
# In collaboration with Datawattch
# Liander Parteon Project Zaandijk

import subprocess
import sys
import time
import traceback
import urllib.error
import os
from typing import List

import json

import numpy as np
import bottleneck as bn

import Packages.RPiGPIO.source.paho.mqtt.publish as publish
import Packages.RPiGPIO.source.paho.mqtt.subscribe as subscribe
import Packages.RPiGPIO.source.paho.mqtt.client as mqtt

from Packages.DataWattch import domoticz, Devices as Dev
from Packages.DataWattch import stm_communication as stm

# Get our serial number
f = open("/home/ubuntu/.panoid")
serial = f.read(14)
f.close()

window_average_out0 = 0
window_average_out1 = 0
window_average_out2 = 0

hp_on_time = 0.0
hp_off_time = 0.0

# Arrays to store power values for averaging
arr_g0 = []
arr_g1 = []
arr_g2 = []

def domoticz_wrapper(function, *args, **kwargs):
    for i in range(10):
        try:
            ret = function(*args, **kwargs)
        except urllib.error.URLError as e:
            print('Unhandled exeption:', type(e), e)
        else:
            return ret
        time.sleep(10)
    sys.exit(1)

# Calculate average power function
def averagepwr (arr):
    window_size = len(arr)          # For 15 minutes every 2 seconds = 450 max
    tot=0
    for i in range(len(arr)):
        tot+=arr[i]
    if len(arr) > 0:
        window_average = round(tot / window_size)
        print(window_average)
        return window_average

# Add elements to array for average power calculation
def add_to_array(arr_in, pwr, max_size):
    if len(arr_in) > max_size:
        for i in range(max_size, len(arr_in)):
            arr_in.pop(i)
    arr_in.insert(0, pwr)
    print('This is the array:', arr_in)

comm_time = False

def on_message(mqttc, obj, msg):
    global temp1_sensorpin 
    global comm_time
    print(msg.topic)
    print(msg.payload)
    ambienttemp = json.loads(msg.payload)
    print("Ambienttemp:", ambienttemp)
    comm_time = True
    temp1_sensorpin= float(ambienttemp['Indoor ambient temp. (R1T)']) # for real use Indoor ambient temp. (R1T)
    comm_time = False
    print("Temptry:", temp1_sensorpin)
    # Update temperature values in DZ
    print("Temp", temp1_sensorpin)


def on_connect(client, userdata, flags, rc):
    print("Connected flags"+str(flags)+"result code " + str(rc)+ "client1_id")
    client.connected_flag=True

def init_values(dom_class: domoticz, cnt: List[Dev.DeviceCounter]):
    dom_class.init_counter_values(cnt)
    pass

if __name__ == "__main__":
    ##############################################################################################################
    # Initialization
    ##############################################################################################################
    dom_comm = domoticz()
    error = ''

    # New try mqqt subscribe
    client = mqtt.Client()
    client.connect("192.168.4.1", 1883, 3)
    topic = "espaltherma/ATTR"
    client.subscribe(topic, 0)
    client.on_message = on_message
    client.on_connect = on_connect
    client.loop_start()

    global temp1_sensorpin
    # Get Domoticz User Variables
    relay = domoticz_wrapper(dom_comm.get_relay, "relay")
    temp1_sensorpin = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp1_pin"))
    temp2_sensorpin = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp2_pin"))
    temp_min = float(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_min"))
    temp_max = float(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_max"))
    temp_time = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_time"))

    pwr_max = float(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_max"))
    pwr_min = float(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_min"))
    pwr_time = int(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_time"))

    (counters, devices) = domoticz_wrapper(dom_comm.get_settings, )
    for i in counters:
        print(i.__dict__)

    (volt, power, gas) = domoticz_wrapper(dom_comm.get_P1_init, )
    init_values(dom_comm, counters)
    outphase = [Dev.DeviceOutputSimple(0), Dev.DeviceOutputSimple(1)]
    Dev.send_init(["output"])
    level = 0
    t_start = 0
    t_startLED = 0
    rly_state = 0
    amp = [0, 0, 0]
    avg_pwr = [0, 0, 0]
    
    sum_averages = 0
    mAver = 0
    window = 1

    # Set current epoch time at startup
    hp_on_time = time.time()

    sentPushEarlier = False

    # Set IDX's based on User Variables
    try:
        idx_temp1 = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp1_idx"))
        idx_temp2 = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp2_idx"))
        idx_relay = int(domoticz_wrapper(dom_comm.get_uservarbyname, "relay_idx"))
    except TypeError as err:
        raise SystemExit("DW Fatal: Temperature and/or Relay IDX not defined in Domoticz. Temp2 MUST be defined.")

    while True:

        # Sleep for 2 seconds at the start of each iteration
        print()
        print("#######################################################")
        print("Tick", time.time())
        print("#######################################################")
        print()

        t_end = time.time() + 2
        while time.time() < t_end:
            pass

        for P1_def in range(len(volt)):
            volt[P1_def].value = domoticz_wrapper(dom_comm.get_data, volt[P1_def].idx)

        for P1_def in range(len(power)):
            power[P1_def].value = domoticz_wrapper(dom_comm.get_data, power[P1_def].idx)

        for P1_def in range(len(gas)):
            gas[P1_def].value = domoticz_wrapper(dom_comm.get_data, gas[P1_def].idx)

        # Adding values from P1 port to array and calculating average
        for i in range(len(power)):
            print('This is power:', power[i].value)
            for j in range(len(volt)):
                   if volt[j].fase == power[i].fase:
                        if i == 0 and j == 0: # Vermogen fase 1
                            add_to_array(arr_g0, power[0].value, 450)
                            print('Global arr0:', arr_g0)
                            window_average_out0 = averagepwr(arr_g0)
                            print('Average')
                        if i == 1 and j == 1: # Vermogen fase 2
                            add_to_array(arr_g1, power[1].value, 450)
                            print('Global arr1:', arr_g1)
                            window_average_out1 = averagepwr(arr_g1)
                        if i == 2 and j == 2: # Vermogen fase 3
                            add_to_array(arr_g2, power[2].value, 450)
                            print('Global arr2:', arr_g2)
                            window_average_out2 = averagepwr(arr_g2)

        for i in range(len(power)):
            for j in range(len(volt)):
                if volt[j].fase == power[i].fase:
                    if volt[j].value > 1000:
                        amp[i] = power[i].value / (volt[j].value / 10.0)
                    else:
                        amp[i] = power[i].value / volt[j].value

                    print('Power:', power[i].value, 'Voltage:', volt[j].value, 'Amps:', amp[i])

        # Averages need to added together and that sum is the limit for 3,4 kW
        sum_averages = window_average_out0 + window_average_out1 + window_average_out2
        print('Sum averages:', sum_averages)
        avg_pwr = sum_averages
        temp2_sensorpin = avg_pwr

        print('HP on time:', hp_on_time)
        print('HP off time:', hp_off_time)

        print()

        domoticz_wrapper(dom_comm.set_temp, idx_temp2, temp2_sensorpin)
        print("Average power", temp2_sensorpin)

        if comm_time == False:
            domoticz_wrapper(dom_comm.set_temp, idx_temp1, temp1_sensorpin)
            print('Temp amb:', temp1_sensorpin)
        else:
            continue


        ##############################################################################################################
        # Event: Turn ON Smart Grid Relay 1 (= Turn OFF heatpump)
        ##############################################################################################################
        # Below temperature limit (but below 150 centigrade; likely faulty thermometer readout)
        switchedByPower = False
        # Above Power limit
        if (avg_pwr > 3400) and (domoticz_wrapper(dom_comm.get_relay, "relay") == False):
            # Check for minimum of 18.9 degrees inside the house
            if (temp1_sensorpin < 150) and (temp1_sensorpin > 18.9) and (domoticz_wrapper(dom_comm.get_relay, "relay") == False):
                if hp_on_time < (time.time() - 1800):
                # # # To do: check if heatpump is on for 30 minutes or more
                    # Switch on the relay (meaning switch off the water heater)
                    # Set expected Relay state to 1
                    level = 1
                    hp_off_time = time.time()
                    # switch command on MQTT:
                    publish.single("espaltherma/sg/set", "1", hostname="192.168.4.1")
                    print("TEMP-RELAY: AAN")
                    domoticz_wrapper(dom_comm.set_switch, idx_relay, "On")
                    # Send a Push Notification via PANINE
                    if switchedByPower:
                        if last_push_sent_at < time.time() - limit_push_notifications:
                            print("Send push that relay is ON")
                            os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=ON")
                            last_push_sent_at = time.time()
                            sentPushEarlier = True

        ##############################################################################################################
        # Events: Turn OFF Relay (= Turn ON Heatpump)
        ##############################################################################################################
        # Below Power Limit AND Above Temperature Limit
        
        if (hp_off_time < (time.time() - 2700)) and (domoticz_wrapper(dom_comm.get_relay, "relay") == True):
                # Set expected Relay state to 0
                level = 0
                hp_on_time = time.time()
                print("switchedByPower=", switchedByPower, " sentPushEarlier=",  sentPushEarlier)
                # switch command on MQTT:
                publish.single("espaltherma/sg/set", "0", hostname="192.168.4.1")
                # Send a Push Notification via PANINE
                if switchedByPower and sentPushEarlier:
                    if last_push_sent_at < time.time() - limit_push_notifications:
                        print("Send push that relay is OFF")
                        os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=OFF")
                        last_push_sent_at = time.time()
                        sentPushEarlier = False

                print("RELAY: UIT")
                domoticz_wrapper(dom_comm.set_switch, idx_relay, "Off")

        ##############################################################################################################
        # Code to run every tick (every 2 seconds)
        ##############################################################################################################
        # Set the relay to the state we expect it to be
        outphase[0].set_level(level)

        print()
        print("Relay (according to var):", level)
        print("Relay (according to DZ): ", domoticz_wrapper(dom_comm.get_relay, "relay"))

        # Update kWh values in Domoticz
        for Dev in range(len(counters)):
            error = counters[Dev].get_counter_value()
            if counters[Dev].update > 0:
                domoticz_wrapper(dom_comm.set_counter, counters[Dev].idx, counters[Dev].update)
                counters[Dev].update = 0
            if error:
                domoticz_wrapper(dom_comm.add_log, error)
            # time.sleep(0.1)
