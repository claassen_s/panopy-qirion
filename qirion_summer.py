# Version RC1.0
# First RC version for Summer switching for Solar Panels
# Python 3
# By Stijn Claassen
#   ____  _      _             
#  / __ \(_)    (_)            
# | |  | |_ _ __ _  ___  _ __  
# | |  | | | '__| |/ _ \| '_ \ 
# | |__| | | |  | | (_) | | | |
#  \___\_\_|_|  |_|\___/|_| |_|
# Qirion Energy Consulting
# In collaboration with Datawattch
# Liander Parteon Project Zaandijk
# Slim Sturen

import subprocess
import sys
import time
import traceback
import urllib.error
import os
from typing import List

import logging

import json

import Packages.RPiGPIO.source.paho.mqtt.publish as publish
import Packages.RPiGPIO.source.paho.mqtt.subscribe as subscribe
import Packages.RPiGPIO.source.paho.mqtt.client as mqtt

from Packages.DataWattch import domoticz, Devices as Dev

# Get our serial number
f = open("/home/ubuntu/.panoid")
serial = f.read(14)
f.close()

window_average_out0 = 0
window_average_out1 = 0
window_average_out2 = 0

window_average_Voutph1 = 0.0
window_average_Voutph2 = 0.0
window_average_Voutph3 = 0.0

hp_on_time = 0.0
hp_off_time = 0.0

# Arrays to store power values for averaging
arr_Pph1 = []
arr_Pph2 = []
arr_Pph3 = []

arr_Vph1 = []
arr_Vph2 = []
arr_Vph3 = []

# Function to catch any Domoticz communication errors/timeouts
def domoticz_wrapper(function, *args, **kwargs):
    for i in range(10):
        try:
            ret = function(*args, **kwargs)
        except urllib.error.URLError as e:
            print('Unhandled exeption:', type(e), e)
            logging.error("Unhandled exeption")
        else:
            return ret
        time.sleep(10)
    sys.exit(1)

# Calculate average function
def averagepwr (arr):
    window_size = len(arr)          # For 15 minutes every 2 seconds = 450 max
    tot=0
    for i in range(len(arr)):
        tot+=arr[i]
    if len(arr) > 0:
        window_average = round(tot / window_size)
        print(window_average)
        return window_average

# Add elements to array for average power calculation
def add_to_array(arr_in, pwr, max_size):
    if len(arr_in) > max_size:
        for i in range(max_size, len(arr_in)):
            arr_in.pop(i)
    arr_in.insert(0, pwr)

comm_time = False

def on_message(mqttc, obj, msg):
    global temp1_sensorpin 
    global comm_time
    print(msg.topic)
    print(msg.payload)
    logging.debug("MQTT topic: {a} | MQTT payload: {b}".format(a=msg.topic, b=msg.payload))
    ambienttemp = json.loads(msg.payload)
    print("Ambienttemp:", ambienttemp)
    logging.debug("Received from HP: {a}".format(a=ambienttemp))
    comm_time = True
    temp1_sensorpin= float(ambienttemp['Indoor ambient temp. (R1T)']) # for real use Indoor ambient temp. (R1T)
    comm_time = False

def on_connect(client, userdata, flags, rc):
    print("Connected flags"+str(flags)+"result code " + str(rc)+ "client1_id")
    client.connected_flag=True

def init_values(dom_class: domoticz, cnt: List[Dev.DeviceCounter]):
    dom_class.init_counter_values(cnt)
    pass

if __name__ == "__main__":
    ##############################################################################################################
    # Initialization
    ##############################################################################################################
    dom_comm = domoticz()
    error = ''

    # Init for logfile, change loglevel according to needs, standard is INFO and DEBUG gives all the DEBUG info
    # Logfile is found in: mypanine/qirion_summer.log and can be read with "tail -f mypanine/qirion_summer.log"
    logging.basicConfig(filename='qirion_summer.log',
                        format='%(asctime)s :: %(levelname)s :: %(message)s',
                        level=logging.INFO)

    # Make sure that the correct devices are enabled
    # Does require an update to panopy-qirion/Packages/Datawattch/domoticz_communication.py
    # Delivery phases are idx: 13, 14 and 15
    domoticz_wrapper(dom_comm.set_active, 13, ) # Delivery L1
    domoticz_wrapper(dom_comm.set_active, 14, ) # Delivery L2
    domoticz_wrapper(dom_comm.set_active, 15, ) # Delivery L3

    # Init to connect to MQTT broker on Raspberry Pi
    client = mqtt.Client()
    client.connect("192.168.4.1", 1883, 3)
    topic = "espaltherma/ATTR"
    client.subscribe(topic, 0)
    client.on_message = on_message
    client.on_connect = on_connect
    client.loop_start()

    global temp1_sensorpin

    # Get Domoticz User Variables
    relay = domoticz_wrapper(dom_comm.get_relay, "relay")
    temp1_sensorpin = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp1_pin"))
    temp2_sensorpin = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp2_pin"))
    temp_min = float(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_min"))
    temp_max = float(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_max"))
    temp_time = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp_time"))

    pwr_max = float(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_max"))
    pwr_min = float(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_min"))
    pwr_time = int(domoticz_wrapper(dom_comm.get_uservarbyname, "amp_time"))

    (counters, devices) = domoticz_wrapper(dom_comm.get_settings, )
    for i in counters:
        print(i.__dict__)

    (volt, power, gas) = domoticz_wrapper(dom_comm.get_P1_init, )
    init_values(dom_comm, counters)
    outphase = [Dev.DeviceOutputSimple(0), Dev.DeviceOutputSimple(1)]
    Dev.send_init(["output"])
    
    # Init of all used variables
    level = 0
    rly_state = 0
    avg_pwr = [0, 0, 0]

    powerph1 = 0
    powerph2 = 0
    powerph3 = 0
    deliveryph1 = 0
    deliveryph2 = 0
    deliveryph3 = 0

    voltph1 = 0.0
    voltph2 = 0.0
    voltph3 = 0.0
    total_voltage = 0.0
    
    sum_averages = 0
    mAver = 0
    window = 1

    # Set current epoch time at startup
    hp_on_time = time.time()

    switchVoltage = False
    sentPushEarlier = False
    message = True

    # Set IDX's based on User Variables
    try:
        idx_temp1 = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp1_idx"))
        idx_temp2 = int(domoticz_wrapper(dom_comm.get_uservarbyname, "temp2_idx"))
        idx_relay = int(domoticz_wrapper(dom_comm.get_uservarbyname, "relay_idx"))
    except TypeError as err:
        raise SystemExit("DW Fatal: Temperature and/or Relay IDX not defined in Domoticz. Temp2 MUST be defined.")

    logging.info("Initialisation finished")

    while True:

        # Sleep for 2 seconds at the start of each iteration
        print()
        print("#######################################################")
        print("Tick", time.time())
        print("#######################################################")
        print()

        logging.debug("Timestamp: {a}".format(a=time.time()))

        t_end = time.time() + 2
        while time.time() < t_end:
            pass

        # Fill Voltage array from Domoticz build in P1 reader
        for P1_def in range(len(volt)):
            volt[P1_def].value = domoticz_wrapper(dom_comm.get_data, volt[P1_def].idx)

        # Fill power array from Domoticz build in P1 reader
        for P1_def in range(len(power)):
            power[P1_def].value = domoticz_wrapper(dom_comm.get_data, power[P1_def].idx)
        
        # Substracting values from P1 port usage to array and calculating average, this takes delivery of power into account
        for i in range(len(power)):
            print('Length of power:', len(power))
            print('This is power:', power[i].value)
            print('Phase:', power[i].fase)
            logging.debug("Length of power: {a} | Current power: {b} | Power phase: {c}".format(a=(len(power)), b=(power[i].value), c=(power[i].fase)))
            if i == 0: # Vermogen fase 1
                powerph1 = power[0].value
            if i == 1: # Vermogen fase 2
                powerph2 = power[1].value
            if i == 2: # Vermogen fase 3
               powerph3 = power[2].value
            if i == 3: # Levering fase 1
                deliveryph1 = power[3].value
                sumPph1 = powerph1 - deliveryph1 # Substracts delivery from power phase 1
                add_to_array(arr_Pph1, sumPph1, 450) # Adds sum to average array, 450 is 450 seconds times 2 (every tick is approx. 2 seconds) 900 seconds = 15 minutes average
                print('Global arr_Pph1:', arr_Pph1)
                logging.debug("Array of power ph1: {a} ".format(a=arr_Pph1))
                window_average_out0 = averagepwr(arr_Pph1)
            if i == 4: # Levering fase 2
                deliveryph2 = power[4].value
                sumPph2 = powerph2 - deliveryph2
                add_to_array(arr_Pph2, sumPph2, 450)
                print('Global arr_Pph2:', arr_Pph2)
                logging.debug("Array of power ph2: {a} ".format(a=arr_Pph2))
                window_average_out1 = averagepwr(arr_Pph2)
            if i == 5: # Levering fase 3
                deliveryph3 = power[5].value
                sumPph3 = powerph3 - deliveryph3
                add_to_array(arr_Pph3, sumPph3, 450)
                print('Global arr_Pph3:', arr_Pph3)
                logging.debug("Array of power ph3: {a} ".format(a=arr_Pph3))
                window_average_out2 = averagepwr(arr_Pph3)
        
        # Averages need to added together and that sum is the limit for -2000 W
        sum_averages = window_average_out0 + window_average_out1 + window_average_out2
        print('Sum averages:', sum_averages)
        logging.debug("Sum power averages: {a} ".format(a=sum_averages))
        avg_pwr = sum_averages
        temp2_sensorpin = avg_pwr

        # Looking at Voltages
        for i in range(len(volt)):
            print('Length of volt:', len(volt))
            print('This is volt:', volt[i].value)
            print('Phase:', volt[i].fase)
            logging.debug("Length of Volt: {a} | Current Volt: {b} | Power Volt: {c}".format(a=len(volt), b=volt[i].value, c=volt[i].fase))
            if i == 0: # Voltage phase 1
                if volt[0].value > 1000: # Compare if voltage has a decimal, it is not showed as decimal but as 4 numbers
                    voltph1 = (volt[0].value / 10.0) # Make decimal from big number
                else:
                    voltph1 = volt[0].value
                add_to_array(arr_Vph1, voltph1, 450)
                print('Global arr_Vph1:', arr_Vph1)
                logging.debug("Array of Volt ph1: {a} ".format(a=arr_Vph1))
                window_average_Voutph1 = averagepwr(arr_Vph1)
            if i == 1: # Voltage phase 2
                if volt[1].value > 1000:
                    voltph2 = (volt[1].value / 10.0)
                else:
                    voltph2 = volt[1].value
                add_to_array(arr_Vph2, voltph2, 450)
                print('Global arr_Vph2:', arr_Vph2)
                window_average_Voutph2 = averagepwr(arr_Vph2)
                logging.debug("Array of Volt ph2: {a} ".format(a=arr_Vph2))
            if i == 2: # Voltage phase 3
                if volt[2].value > 1000:
                    voltph3 = (volt[2].value / 10.0)
                else:
                    voltph3 = volt[2].value
                add_to_array(arr_Vph3, voltph3, 450)
                print('Global arr_Vph3:', arr_Vph3)
                logging.debug("Array of Volt ph3: {a} ".format(a=arr_Vph3))
                window_average_Voutph3= averagepwr(arr_Vph3)

        total_voltage = (window_average_Voutph1 + window_average_Voutph2 + window_average_Voutph3) / 3.0

        # Looks if Voltage exceeds 249 Volts
        print('Total voltage:', total_voltage)
        if total_voltage > 249:
            print('Set switchVoltage true')
            logging.debug("Voltage exceeds 249 Volts")
            logging.debug("Set switchVoltage true")
            switchVoltage = True
        else:
            switchVoltage = False
            message == True
            logging.debug("Set switchVoltage false")

        # Debug messages for different variables
        print('switchVoltage:', switchVoltage)
        logging.debug("switchVoltage: {a} ".format(a=switchVoltage))

        print('HP on time:', hp_on_time)
        print('HP off time:', hp_off_time)
        logging.debug("HP on time: {a} | HP off time: {b}".format(a=hp_on_time, b=hp_off_time))

        domoticz_wrapper(dom_comm.set_temp, idx_temp2, temp2_sensorpin)
        print("Average power", temp2_sensorpin)
        logging.debug("Average power: {a} ".format(a=temp2_sensorpin))

        # If MQTT message is received, write new temperature value to Domoticz
        if comm_time == False:
            domoticz_wrapper(dom_comm.set_temp, idx_temp1, temp1_sensorpin)
            print('Temp amb:', temp1_sensorpin)
            logging.debug("Ambient room temperature: {a} ".format(a=temp1_sensorpin))
        else:
            continue


        ##############################################################################################################
        # Event: Turn ON Smart Grid Relay 2 (= Turn ON heatpump) [ 1 0 ]
        ##############################################################################################################
        switchedByPower = False
        # Above Power limit (-2000 Watts) AND above Voltage (249 Volts) limit
        if (avg_pwr < -2000) and (switchVoltage == True) and (domoticz_wrapper(dom_comm.get_relay, "relay") == False):
            # Switch on the relay
            # Set expected Relay state to 1
            level = 1
            hp_off_time = time.time()
            # Switch command on MQTT:
            publish.single("espaltherma/sg/set", "2", hostname="192.168.4.1")
            print("TEMP-RELAY: AAN")
            logging.debug("TEMP-RELAY: AAN")
            logging.info("Warmtepomp aanbevolen AAN voor komende 15 minuten")
            logging.info("Binnentemperatuur op moment van schakelen: {a}".format(a=temp1_sensorpin))
            domoticz_wrapper(dom_comm.set_switch, idx_relay, "On")
            # Send a Push Notification via PANINE
            if switchedByPower:
                if last_push_sent_at < time.time() - limit_push_notifications:
                    print("Send push that relay is ON")
                    logging.debug("Send push that relay is ON")
                    os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=ON")
                    last_push_sent_at = time.time()
                    sentPushEarlier = True

        ##############################################################################################################
        # Event: Turn OFF Relay (= Normal Mode) [ 0 0 ]
        ##############################################################################################################
        # After 15 seconds HP in normal mode again
        if (hp_off_time < (time.time() - 900)) and (avg_pwr > -2000) and (domoticz_wrapper(dom_comm.get_relay, "relay") == True):
            # Set expected Relay state to 0
            level = 0
            hp_on_time = time.time()
            print("switchedByPower=", switchedByPower, " sentPushEarlier=",  sentPushEarlier)
            logging.debug("switchedByPower: {a} | sentPushEarlier: {b}".format(a=switchedByPower, b=sentPushEarlier))
            # switch command on MQTT:
            publish.single("espaltherma/sg/set", "0", hostname="192.168.4.1")
            # Send a Push Notification via PANINE
            if switchedByPower and sentPushEarlier:
                if last_push_sent_at < time.time() - limit_push_notifications:
                    print("Send push that relay is OFF")
                    logging.debug("Send push that relay is OFF")
                    os.system("php /home/ubuntu/mypanine/artisan p:push RELAY=OFF")
                    last_push_sent_at = time.time()
                    sentPushEarlier = False
            print("RELAY: UIT")
            logging.debug("TEMP-RELAY: OFF")
            logging.info("Warmtepomp naar normaal bedrijf")
            domoticz_wrapper(dom_comm.set_switch, idx_relay, "Off")

        ##############################################################################################################
        # Code to run every tick (every 2 seconds)
        ##############################################################################################################
        # Set the relay to the state we expect it to be
        outphase[0].set_level(level)

        # print()
        print("Relay (according to var):", level)
        logging.debug("Relay (according to var): {a}".format(a=level))
        print("Relay (according to DZ): ", domoticz_wrapper(dom_comm.get_relay, "relay"))
        logging.debug("Relay (according to DX): {a}".format(a=domoticz_wrapper(dom_comm.get_relay, "relay")))

        # Update kWh values in Domoticz
        for Dev in range(len(counters)):
            error = counters[Dev].get_counter_value()
            if counters[Dev].update > 0:
                domoticz_wrapper(dom_comm.set_counter, counters[Dev].idx, counters[Dev].update)
                counters[Dev].update = 0
            if error:
                domoticz_wrapper(dom_comm.add_log, error)
# End of file!